package com.Runner;

import org.junit.runner.RunWith;
import cucumber.api.CucumberOptions;
import net.serenitybdd.cucumber.CucumberWithSerenity;

@RunWith(CucumberWithSerenity.class)

@CucumberOptions(
features = "src/test/resources/features/ChecklistAutomatizado.feature", 
tags= {"@ID1"},
glue = {"com.StepsDefinition" })

public class Runner {

}
