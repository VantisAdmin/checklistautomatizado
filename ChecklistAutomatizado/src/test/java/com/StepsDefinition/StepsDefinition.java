package com.StepsDefinition;

import com.Definition.Definition;

import cucumber.api.java.en.Given;
import cucumber.api.java.en.And;
import net.thucydides.core.annotations.Steps;

public class StepsDefinition {

	@Steps
	Definition _Definition;


	@Given("^Abrir url$")
	public void Abrir_url() throws Exception {
		_Definition.Abrir_url();
	
	}
	
	@And("^Buscar producto$")
	public void Buscar_producto() throws Exception {
		
		_Definition.Buscar_producto();
	}
	
	
}
