package com.Definition;

import com.Utils.WebElements;
import com.Utils.Utils;

import net.thucydides.core.annotations.Step;

public class Definition {

	WebElements _Element;
	Utils _Util;

	@Step
	public void Abrir_url() {
		try {
			_Element.open();
		} catch (Exception ex) {
			System.out.print(ex.toString());
		}

	}

	@Step
	public void Buscar_producto() {
		try {
			_Element.txtSearch.sendKeys("Disney");
			_Util.ThreadSleep(10);
		} catch (Exception ex) {
			System.out.print(ex.toString());
		}

	}
}
